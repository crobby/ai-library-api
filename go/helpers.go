package ailibraryapi

import (
	"crypto/tls"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
)

func GetSeldonPrediction(endpoint string, request string) (PredictionResult, error) {
	var respObject PredictionResult
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	resp, err := http.Post(endpoint, "application/json", strings.NewReader(request))
	if err == nil && resp != nil && resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return PredictionResult{}, err
		}
		bodyString := string(bodyBytes)
		if err := json.Unmarshal([]byte(bodyString), &respObject); err != nil {
			return PredictionResult{}, err
		}
	}
	return respObject, err
}
