/*
 * AI Library
 *
 * This is the API definition for interacting with models served by the AI Library.
 *
 * API version: 0.0.1
 * Contact: croberts@redhat.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package ailibraryapi

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/getkin/kin-openapi/openapi3filter"
)

// Predictions - Submit a request to the linear-regression model
func Lrpredictions(w http.ResponseWriter, r *http.Request) {
	ctx := context.TODO()
	rtr := openapi3filter.NewRouter().WithSwaggerFromFile("swagger.yaml")
	route, pathParams, _ := rtr.FindRoute(r.Method, r.URL)
	requestValidationInput := &openapi3filter.RequestValidationInput{
		Request:    r,
		PathParams: pathParams,
		Route:      route,
	}
	if err := openapi3filter.ValidateRequest(ctx, requestValidationInput); err != nil {
		http.Error(w, fmt.Sprintf("%v\n", err), http.StatusBadRequest)
		return
	}

	jsonString, err := LinearRegressionPrediction(w, r)
	if err != nil {
		http.Error(w, fmt.Sprintf("Unable to produce response %v", err), http.StatusBadRequest)
		return
	}

	responseValidationInput := &openapi3filter.ResponseValidationInput{
		RequestValidationInput: requestValidationInput,
		Status:                 200,
		Header: http.Header{
			"Content-Type": []string{
				"application/json; charset=UTF-8",
			},
		},
	}
	jsonResp, _ := json.Marshal(jsonString)
	responseValidationInput.SetBodyBytes(jsonResp)
	if err := openapi3filter.ValidateResponse(ctx, responseValidationInput); err != nil {
		http.Error(w, fmt.Sprintf("%v\n", err), http.StatusInternalServerError)
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(jsonResp)
}

// Predictions - Submit a request to the linear-regression model
func LinearRegressionPrediction(w http.ResponseWriter, r *http.Request) (PredictionResult, error) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("Parse body failed: %v", err), http.StatusBadRequest)
	}
	var predictionRequest LinearRegressionRequest
	err = json.Unmarshal(body, &predictionRequest)
	log.Printf("The input data is: %v\n", predictionRequest)
	linearRegressionEndpoint := os.Getenv("LRENDPOINT")
	reqbody := GetSeldonLRRequest(predictionRequest)
	return GetSeldonPrediction(linearRegressionEndpoint, reqbody)
}

func GetSeldonLRRequest(request LinearRegressionRequest) string {
	s3accesskey := os.Getenv("S3KEY")
	s3secretkey := os.Getenv("S3SECRET")
	s3Endpoint := os.Getenv("S3ENDPOINT")
	reqbody := "{strData: \"s3endpointUrl=" + s3Endpoint + ", s3accessKey=" + s3accesskey + ", s3secretKey=" + s3secretkey +
		", s3objectStoreLocation=DH-DEV-DATA" +
		", s3Path=healthpredictor" +
		", model=model.pkl" +
		", inputdata=" + fmt.Sprintf("%f", request.PredictionData[0]) + ":" + fmt.Sprintf("%f", request.PredictionData[1]) + "\"}"
	return reqbody
}
