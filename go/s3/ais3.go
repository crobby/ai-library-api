package s3

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"mime/multipart"
	"os"
)

func UploadDataSet(filename string, contents multipart.File) {
	bucket := os.Getenv("S3BUCKET")
	s3Endpoint := os.Getenv("S3ENDPOINT")
	sess := createSession(s3Endpoint)
	svc := s3manager.NewUploader(sess)

	result, err := svc.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(filename),
		Body:   contents,
	})
	if err != nil {
		fmt.Println("Error uploading to S3 storage", err)
	}
	fmt.Printf("Successfully uploaded %s to %s\n", filename, result.Location)
}

func createSession(customEndpoint string) *session.Session {
	s3accesskey := os.Getenv("S3KEY")
	s3secretkey := os.Getenv("S3SECRET")
	defaultResolver := endpoints.DefaultResolver()
	s3CustResolverFn := func(service, region string, optFns ...func(*endpoints.Options)) (endpoints.ResolvedEndpoint, error) {
		if service == "s3" {
			return endpoints.ResolvedEndpoint{
				URL:           customEndpoint,
				SigningRegion: "custom-signing-region",
			}, nil
		}

		return defaultResolver.EndpointFor(service, region, optFns...)
	}
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			EndpointResolver: endpoints.ResolverFunc(s3CustResolverFn),
			Credentials:      credentials.NewStaticCredentials(s3accesskey, s3secretkey, ""),
		},
	}))
	return sess
}
