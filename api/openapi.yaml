openapi: 3.0.0
info:
  contact:
    email: croberts@redhat.com
  description: This is the API definition for interacting with models served by the
    AI Library.
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  title: AI Library
  version: 0.0.1
servers:
- url: /
tags:
- description: Linear Regression model
  name: Linear Regression
paths:
  /prediction/linear-regression:
    post:
      operationId: lrpredictions
      parameters: []
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LinearRegressionRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PredictionResult'
          description: predictionResults - json representation of the prediction result
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the linear-regression model
      tags:
      - linear-regression
  /prediction/fraud-detection:
    post:
      operationId: fdpredictions
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/FraudDetectionRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PredictionResult'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the fraud detection model
      tags:
      - fraud-detection
  /prediction/associative-rule-learning:
    post:
      operationId: arlpredictions
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/AssociativeRuleLearningRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PredictionResult'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the associative rule learning model
      tags:
      - associative-rule-learning
  /prediction/correlation-analysis:
    post:
      operationId: capredictions
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/CorrelationAnalysisRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PredictionResult'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the correlation analysis model
      tags:
      - correlation-analysis
  /prediction/duplicate-bug-predict:
    post:
      operationId: predictdb
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/DuplicateBugPredictRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PredictionResult'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the duplicate bug model
      tags:
      - duplicate-bug-predict
  /prediction/duplicate-bug-train:
    post:
      operationId: traindb
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/DuplicateBugTrainRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PredictionResult'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a training request to the duplicate bug model
      tags:
      - duplicate-bug-train
  /prediction/flakes-predict:
    post:
      operationId: pedictflakes
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/FlakesPredictRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PredictionResult'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the flakes model
      tags:
      - flakes-predict
  /prediction/flakes-train:
    post:
      operationId: trainflakes
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/FlakesTrainRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PredictionResult'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a training request to the flakes model
      tags:
      - flakes-train
  /prediction/matrix-factorization:
    post:
      operationId: mfpredictions
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/MatrixFactorizationRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PredictionResult'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the matrix factorization model
      tags:
      - matrix-factorization
  /prediction/topic-model:
    post:
      operationId: tmpredictions
      parameters: []
      requestBody:
        content:
          'multipart/form-data:':
            schema:
              $ref: '#/components/schemas/TopicModelRequest'
        description: destination - location to store the prediction results predictionData
          - prediction dataset file
        required: true
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PredictionResult'
          description: uuid - uuid where the uploaded data exists data - other information
            regarding location of predictions (json)
        400:
          description: Invalid input
      security: []
      summary: Submit a request to the topic model
      tags:
      - topic-model
components:
  callbacks: {}
  links: {}
  requestBodies: {}
  schemas:
    LinearRegressionRequest:
      example:
        predictionData:
        - 0.8008282
        - 0.8008282
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    FraudDetectionRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    AssociativeRuleLearningRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    CorrelationAnalysisRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    DuplicateBugPredictRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    DuplicateBugTrainRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    FlakesTrainRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    FlakesPredictRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    MatrixFactorizationRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    TopicModelRequest:
      properties:
        predictionData:
          items:
            format: float
            type: number
          type: array
      required:
      - predictionData
      type: object
    PredictionResult:
      example:
        data:
          names:
          - names
          - names
          tensor:
            shape:
            - 0.8008281904610115
            - 0.8008281904610115
            values:
            - 6.0274563
            - 6.0274563
        meta:
          routing: '{}'
          puid: puid
          metrics:
          - metrics
          - metrics
          requestpath: '{}'
          tags: '{}'
      properties:
        meta:
          $ref: '#/components/schemas/PredictionResult_meta'
        data:
          $ref: '#/components/schemas/PredictionResult_data'
      type: object
    PredictionResult_meta:
      example:
        routing: '{}'
        puid: puid
        metrics:
        - metrics
        - metrics
        requestpath: '{}'
        tags: '{}'
      properties:
        puid:
          type: string
        tags:
          type: object
        routing:
          type: object
        requestpath:
          type: object
        metrics:
          items:
            type: string
          type: array
    PredictionResult_data_tensor:
      example:
        shape:
        - 0.8008281904610115
        - 0.8008281904610115
        values:
        - 6.0274563
        - 6.0274563
      properties:
        shape:
          items:
            type: number
          type: array
        values:
          items:
            format: float
            type: number
          type: array
    PredictionResult_data:
      example:
        names:
        - names
        - names
        tensor:
          shape:
          - 0.8008281904610115
          - 0.8008281904610115
          values:
          - 6.0274563
          - 6.0274563
      properties:
        names:
          items:
            type: string
          type: array
        tensor:
          $ref: '#/components/schemas/PredictionResult_data_tensor'
  securitySchemes: {}
